const path = require('path')
const webpack = require('webpack')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

// Phaser webpack config
const phaserModule = path.join(__dirname, '/node_modules/phaser/')
const phaser = path.join(phaserModule, 'src/phaser.js')

module.exports = {
  mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
  target: process.env.NODE_ENV === 'production' ? 'production' : ['web', 'es5'],
  entry: {
    app: [
      path.resolve(__dirname, 'src/main.js')
    ]
  },
  output: {
    path: path.resolve(__dirname, 'public'),
    publicPath: './',
    filename: '[name].js'
  },
  resolve: {
    extensions: ['.js', '.ts'],
    alias: {
      assets: path.join(__dirname, './src/assets/')
    }
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './src/index.html',
      chunks: ['vendor', 'app'],
      chunksSortMode: 'manual',
      minify: {
        removeAttributeQuotes: true,
        collapseWhitespace: true,
        html5: true,
        minifyCSS: true,
        minifyJS: true,
        minifyURLs: true,
        removeComments: true,
        removeEmptyAttributes: true
      },
      hash: true
    }),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: './node_modules/phaser/dist/phaser.min.js',
          to: 'lib'
        },
        {
          from: './src/assets',
          to: 'assets',
          globOptions: {
            ignore: ['*.md', '*.tmx']
          }
        }
      ]
    })

  ],
  module: {
    rules: [
      { test: /\.js$/, use: ['babel-loader'], include: path.join(__dirname, 'src') },
      { test: /phaser-split\.js$/, use: 'raw-loader' },
      { test: [/\.vert$/, /\.frag$/], use: 'raw-loader' }
    ]
  },
  optimization: {
    minimize: true
  }
}
